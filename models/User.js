const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "firstname is required"]
	},
	lastName: {
		type: String,
		required: [true, "lastname is required"]
	},
	email: {
		type: String,
		required: [true, "email is required"]
	},
	password: {
		type: String,
		required: [true, "password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [false, "mobile number is optional"]
	},
	userImageUrl: {
		type: String,
		required: [false, "optional"]
	},
	loyalty: {
		type: String,
		required: [false, "optional"]
	},
	reserve1: {
		type: String,
		required: [false, "Optional"]
	},
	reserve2: {
		type: String,
		required: [false, "Optional"]
	},
	reserve3: {
		type: String,
		required: [false, "Optional"]
	},
	reserve4: {
		type: String,
		required: [false, "Optional"]
	},
	reserve5: {
		type: String,
		required: [false, "Optional"]
	},
	address: {
		type: String,
		required: [false, "address is optional"]
	},
	// The "enrollments" property/field will be an array of objects containing the course IDs, the date and time that the user enrolled to the course and the status that indicates if the user is currently enrolled to a course
	orders: [
		{
			productId: {
				type: String,
				required: [true, "Product Id is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Ordered"
			}
		}
	]
})


module.exports = mongoose.model("User", userSchema);