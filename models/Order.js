const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		required: [true, "Buyer is required"]
	},
	reserve1: {
		type: String,
		required: [false, "Optional"]
	},
	reserve2: {
		type: String,
		required: [false, "Optional"]
	},
	reserve3: {
		type: String,
		required: [false, "Optional"]
	},
	reserve4: {
		type: String,
		required: [false, "Optional"]
	},
	reserve5: {
		type: String,
		required: [false, "Optional"]
	},
	products: [{
		productId: {
			type: String
		},
		name: String,
		quantity: {
			type: Number, 
			required: true,
			min: [1, 'Minimum 1'],
			default: 1
		},
		price: {
			type: Number,
			required: true
		}
	}],
	total: {
		type: Number,
		required: [false, "Price is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})


module.exports = mongoose.model("Order", orderSchema);

