const User = require('../models/User');
const Product = require('../models/Product');
const Cart = require('../models/Cart');



module.exports.getCart = (payload) => {

	return Cart.find({userId: payload.id}).then(result => {
		return result;
	})
}

module.exports.addToCart = async (data) => {
	const {productId, quantity, cartUrl} = data.reqBody;

	// returns false if not found
	let cart = await Cart.findOne({userId: data.payload.id});
	let product = await Product.findOne({_id: productId});

	if(!product) {
		return 'Product not Found!';
	}
	
	const {price, name} = product;

	// check existing cart
	if(cart) {
		let productIndex = cart.products.findIndex(p => p.productId == productId);

		// check items in the cart
		if(productIndex > -1) {
			let productItem = cart.products[productIndex]; 
			productItem.quantity += quantity;
			cart.products[productIndex] = productItem;
		}
		else {
			cart.products.push({productId: productId, name: name, quantity: quantity, price: price, cartUrl: cartUrl})
		}
		cart.subTotal += price * quantity;

		return cart.save().then((result, err) => {
			if(err) {
				return false;
			}
			else {
				return true;
			}
		})

	}
	else {
		
		// create cart
		cart = new Cart({
			userId: data.payload.id,
			products: [{productId: productId, name: name, quantity: quantity, price: price, cartUrl: cartUrl}],
			subTotal: price * quantity
		});

		return cart.save().then((result, err) => {
			if(err) {
				return false;
			}
			else {
				console.log(cart)
				return true;
			}
		})
	}
}

module.exports.deletesCart = (userId) => {
	return Cart.deleteOne({userId: userId}).then(result => {
		return result;
	})
}


module.exports.removeProduct = async (data) => {
	const {userId, productId} = data;
	let cart = await Cart.findOne({userId: userId});
	let productIndex = cart.products.findIndex(p => p.productId == productId);

	// check's product
	if(productIndex > -1) {
		let productItem = cart.products[productIndex];
		cart.subTotal -= productItem.quantity*productItem.price;
        cart.products.splice(productIndex,1);
	}
	return cart.update(cart).then((result, err) => {
		if(err) {
			return false;
		}
		else {
			return cart;
		}
	})

}

