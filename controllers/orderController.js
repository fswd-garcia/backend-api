const User = require('../models/User');
const Product = require('../models/Product');
const productController = require('../controllers/productController');
const Order = require('../models/Order');
const Cart = require('../models/Cart');
const cartController = require('../controllers/cartController')
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Retrieve all orders (Admin only)
module.exports.getAllOrders = async (data) => {

    if (data.isAdmin) {

        return Order.find({}).then(result => {

            return result

        })
    } else {
        
        return false
    }
}

module.exports.checkout = (payload) => {
    return Cart.findOne({userId: payload.id}).then(result => {
        // search for cart
        if(result) {
            const {userId, products, subTotal} = result;

            let order = new Order({
                userId: userId,
                products: products,
                total: subTotal
            });

            return order.save().then((order, err) => {
                if(err) {
                    return false;
                }
                else {
                    console.log(order.userId);
                    cartController.deletesCart(order.userId);
                    return order;
                }
            })
        }
        else {
            return false;
        }
    })
}

module.exports.getOrderHistory = (payload) => {
    return Order.find({userId: payload.id}).then(result => {
        return result;
    })
}