const Product = require('../models/Product');
const User = require('../models/User');
const ejs = require('ejs');
const bodyParser = require('body-parser');

// Create Product (Admin only)
module.exports.addProduct = (reqBody) => {

    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        quantity: reqBody.quantity,
        imgUrl: reqBody.imgUrl
    });

    return newProduct.save().then((product, error) => {

        if (error) {

            return false

        } else {

            return true
        }
    })
}

/* module.exports.addProduct = (reqBody) => {

    return product.find({name: reqBody.name}).then(result => {

        // The "find" method returns a record if a match is found
        if(result.length > 0) {
            return ("Product name exist")
        
        // No duplicate email found
        // The user is not yet registered in the database
        } else {

    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        quantity: reqBody.quantity,
        imgUrl: reqBody.imgUrl
    });

    return newProduct.save().then((product, error) => {

        if (error) {

            return false

        } else {

            return true
                }
            })
        }
    })    
} */


// Retrieve all active products 
module.exports.getAllActive = () => {

    return Product.find({ isActive: true }).then(result => {

        return result
    })
}

// Addt Feature - Route to Retrieve all products (Admin Only)
module.exports.getAllProducts = () => {

    return Product.find({}).then(result => {
        
        return result

    })
    .catch(err => {
        return ("Failed")
    })
}

// Route to Retrieve single product
module.exports.getProduct = (reqParams) => {

    return Product.findById(reqParams.productId).then((result, error) => {

        if (error) {
            return false

        } else {
            return result
        }
    })
}

// Update Product information (Admin only)
module.exports.updateProduct = (reqParams, reqBody) => {

    let updatedProduct = {

        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        quantity: reqBody.quantity,
        imgUrl: reqBody.imgUrl,
        isActive: reqBody.isActive


    }

    return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

        console.log("product edit");

        if (error) {
            return false

        } else {
            return true
        }
    })

}

// Archive Product (Admin only)
module.exports.deactivateProduct = (reqParams, reqBody) => {

    let deactivateProduct = {

        isActive: reqBody.isActive
    }

    return Product.findByIdAndUpdate(reqParams.productId, deactivateProduct).then((product, error) => {

        if (error) {
            return false

        } else {
            return true
        }
    })

}

//Check if the email already exists
module.exports.checkProductExists = (reqBody) => {

    // The result is sent back to the frontend via the "then" method found in the route file
    return Product.find({name: reqBody.name}).then(result => {

        // The "find" method returns a record if a match is found
        if (result.length > 0) {
            return true

        // No duplicate email found
        // The user is not yet registered in the database
        } else {
            return false
        }

    })

}