const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Order = require('../models/Order');
const orderController = require('../controllers/orderController');

//Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {

    // The result is sent back to the frontend via the "then" method found in the route file
    return User.find({email: reqBody.email}).then(result => {

        // The "find" method returns a record if a match is found
        if (result.length > 0) {
            return true

        // No duplicate email found
        // The user is not yet registered in the database
        } else {
            return false
        }

    })

}


// User Registration 
module.exports.registerUser = (reqBody) => {


    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10),
        address: reqBody.address,
        userImageUrl: reqBody.userImageUrl,
        loyalty: reqBody.loyalty

    })

    return newUser.save().then((user, error) => {

        if (error) {
            return false

        } else {
            return true
        }
    })
}

// User Registration 
module.exports.registerUserwch = (reqBody) => {

    // The result is sent back to the frontend via the "then" method found in the route file
    return User.find({email: reqBody.email}).then(result => {

        // The "find" method returns a record if a match is found
        if(result.length > 0) {
            return ("Email Address already used")
        
        // No duplicate email found
        // The user is not yet registered in the database
        } else {
            let newUser = new User({
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                email: reqBody.email,
                mobileNo: reqBody.mobileNo,
                password: bcrypt.hashSync(reqBody.password, 10)
            })

            return newUser.save().then((user, error) => {

                if (error) {
                    return false

                } else {
                    return (`User ${newUser.firstName} ${newUser.lastName} is now registered`)
                }
            })
        }
    })    
}

// User Authentication 
module.exports.loginUser = (reqBody) => {

    return User.findOne({ email: reqBody.email }).then(result => {

        if (result == null) {
            return false

        } else {

            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if (isPasswordCorrect) {

                return { access: auth.createAccessToken(result) }

            } else {
                return false
            }
        }
    })
}



// Set user as Admin 
module.exports.updateUser = (reqParams, reqBody) => {

	let updateUser = {

		isAdmin: reqBody.isAdmin
	}


	return User.findByIdAndUpdate(reqParams.userId, updateUser).then((user, error) => {

		if(error) {
			return false

		} else {
			return true
		}
	})

}


// Non-admin User checkout (Create Order)  
/* module.exports.order2 = async (data) => {

    return User.findById(data.userId).then(result => {

            return Product.findById(data.productId).then(result => {

                    let isUserUpdated = await User.findById(data.userId).then(user => {

                        user.orders.push({ productId: data.productId });

                        return user.save().then((user, error) => {

                            if (error) {
                                return false
                            } else {

                                return true
                            }

                        })

                    })

                    let isProductUpdated = await Product.findById(data.productId).then(product => {

                        product.buyers.push({ userId: data.userId });

                        return product.save().then((product, error) => {

                            if (error) {
                                return false
                            } else {
                                //return ("Order created by Non Admin User")
                                return true
                            }
                        })

                    })

                    let isOrderUpdated = await Product.findById(data.productId).then(product => {

                        let newOrder = new Order({
                            userId: data.userId,
                            productId: product._id,
                            price: product.price
                        })

                        return newOrder.save().then((user, error) => {

                            if (error) {
                                return false

                            } else {
                                return true
                            }
                        })

                    })

                    if (isUserUpdated && isProductUpdated) {

                        // console.log("buyers "+product.buyers)
                        return true

                    } else {

                        return false

                    }

                    return true
                })
                .catch(err => {
                    return false
                })
        })
        .catch(err => {
            return false
        })

} */

// fork from s37-s41
module.exports.order = async (data) => {

    if (data.isAdmin == true) {

        return false
        
    } else {

        let isUserUpdated = await User.findById(data.userId).then(user => {

                        user.orders.push({ productId: data.productId });

                        return user.save().then((user, error) => {

                            if (error) {
                                return false
                            } else {

                                return true
                            }

                        })

                    })


        let isProductUpdated = await Product.findById(data.productId).then(product => {

                        product.buyers.push({ userId: data.userId });

                        return product.save().then((product, error) => {

                            if (error) {
                                return false
                            } else {
                                //return ("Order created by Non Admin User")
                                return true
                            }
                        })

                    })

        let isOrderUpdated = await Product.findById(data.productId).then(product => {

                        let newOrder = new Order({
                            userId: data.userId,
                            productId: product._id,
                            price: product.price
                        })

                        return newOrder.save().then((user, error) => {

                            if (error) {
                                return false

                            } else {
                                return true
                            }
                        })

                    })
        

        if (isUserUpdated && isProductUpdated) {

            return true 
            
        } else {

            return false
        }

    }

}

// Retrieve authenticated user’s orders
module.exports.detailsUser = (data) => {

    return User.findById(data.id).then((result, error) => {


        if (error) {
            return error
        }
        console.log(result)



        // return result.orders;
        return result;
    })
}

// Retrieve all orders (Admin)
module.exports.getAllUsers = async (data) => {

    if (data.isAdmin) {

        return User.find({}).then(result => {

            return result

        })
    } else {

        return false
    }
}
// user details update
module.exports.updateUserDetails = (data) => {
    const {firstName, lastName, mobileNo} = data.updatedUserDetails;

    return User.findById(data.payload.id).then((result, err) => {
        result.firstName = firstName;
        result.lastName = lastName;
        result.mobileNo = mobileNo;
        result.isAdmin = false; 

        return result.save().then((result, err) => {
            if(err) {
                return false;
            }
            else {
                return result;
            }
        })

    })
}

module.exports.getUser = (payload) => {

    return User.findById(payload.id).then(result => {
        let userData = ({...result}._doc); 
 
        delete userData.password;
        return userData;
    })
}

//! GET SINGLE USER DETAILS [ADMIN]
module.exports.getSingleDetails = (req, res) => {
    console.log(req.body)
    console.log(req.user.id)

    User.findById(req.user.id)
        .then(result => res.send(result))
        .catch(err => res.send(err))
};

// Route to Retrieve single user
module.exports.getUser2 = (reqParams) => {

    return User.findById(reqParams.userId).then((result, error) => {

        if (error) {
            return false

        } else {
            return result
        }
    })
}

// Update Users information (Admin only)
module.exports.updateEditUser = (reqParams, reqBody) => {

    let updatedUser = {

        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        /* password: bcrypt.hashSync(reqBody.password, 10), */
        address: reqBody.address,
        userImageUrl: reqBody.userImageUrl,
        loyalty: reqBody.loyalty

    }

    return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user, error) => {

        console.log("user edit");

        if (error) {
            return false

        } else {
            return true
        }
    })

}