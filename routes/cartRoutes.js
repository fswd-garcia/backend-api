const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cartController');
const auth = require('../auth');


// cart delete
router.delete('/cartdelete', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;

	cartController.deletesCart(userId).then(resultFromController => {
		res.send(resultFromController)
	})
});

// retrieve user's cart
router.get('/', auth.verify, (req, res) => {
	const payload = auth.decode(req.headers.authorization);

	cartController.getCart(payload).then(resultFromController => {
		res.send(resultFromController)
	})
});

// delate an item from the cart
router.delete('/:productId', auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.params.productId
	};

	cartController.removeProduct(data).then(resultFromController => {
		res.send(resultFromController)
	})
});

// create cart, if existing - create item instead. needs quantity and productId
router.post('/', auth.verify, (req, res) => {
	const data = {
		payload: auth.decode(req.headers.authorization),
		reqBody: req.body
		};

	cartController.addToCart(data).then(resultFromController => {
		res.send(resultFromController)
	})
});



module.exports = router;