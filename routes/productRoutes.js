const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require("../auth");


// Route to Create Product (Admin only)
router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    
    if (userData.isAdmin) {

        productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));

    } else {

        res.send({ auth: "failed" });

    }

});

// Route to Retrieve all active products
router.get("/allactiveproducts", (req, res) => {

    productController.getAllActive().then(resultFromController => res.send(resultFromController))

})

// Addt Feature - Route to Retrieve all products (Admin Only)
router.get("/allproducts", (req, res) => {

   const userData = auth.decode(req.headers.authorization);
    
    if (userData.isAdmin) {

        productController.getAllProducts().then(resultFromController => res.send(resultFromController))

    } else {

           res.send({ auth: "failed" });

    }
    
})

// Route to Retrieve single product
router.get("/:productId", (req, res) => {

    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
} )

// Route to Update Product information (Admin only)
router.put("/edit/:productId", auth.verify, (req, res) => {

    productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))

})

// Route for Archive Product (Admin only)
router.put("/:productId/archive", auth.verify, (req, res) => {

            const userData = auth.decode(req.headers.authorization);

            if (userData.isAdmin) {

                productController.deactivateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))

            } else {

                res.send({ auth: "failed" });
    }

})

router.post("/checkProduct", (req, res) => {
    productController.checkProductExists(req.body).then(resultFromController => res.send(resultFromController));
})



module.exports = router;